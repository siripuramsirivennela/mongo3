const groups = require("../models/groups")
module.exports = {
async up(db) {
        return await db.createCollection('groups')
   },
    async down(db) {
        return await db.collection('groups').drop()
    },
}