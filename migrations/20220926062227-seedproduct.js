module.exports = {

  async up(db) {

      return await db.collection('products').insertMany([{
        name: 'shoes',

        description:"A shoe is an item of footwear intended to protect and comfort the human foot.",

        isactive:'true',

        createdAt: new Date(),

        updatedAt: new Date(),

      },{
        name: 'toys',

        description:"A toy or plaything is an object that is used primarily for providing entertainment.",

        isactive:'true',

        createdAt: new Date(),

        updatedAt: new Date(),
      }], {})

  },

  async down(db) {

      return await db.collection('products').deleteMany({})

  },

}