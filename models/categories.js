module.exports = mongoose => {
    const newSchema = new mongoose.Schema({
        name: {
               type: String
               },
        description: {
                type: String
                     },
        groupId:{
                type:integer
               },
        Isactive: {
               type: Boolean
            }
        }, {
            timestamps: {
            createdAt: 'created_at',
             updatedAt: 'updated_at'
              }
            });
    
        //newSchema.plugin(autoIncrement.plugin, 'groups');
    
        const categories = mongoose.model('categories', newSchema);
    
        return categories;
    
    };