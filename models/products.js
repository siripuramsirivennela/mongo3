//const autoIncrement = require('mongoose-auto-increment');

module.exports = mongoose => {

    const newSchema = new mongoose.Schema({

        name: {

            type: String

        },

        description: {

            type: String

        },

        image_url: {

            type: String

        },

        categoriesid:{

            type:integer

        },

        Isactive: {

            type: Boolean

        }

    }, {

        timestamps: {

            createdAt: 'created_at',

            updatedAt: 'updated_at'

        }

    });

    //newSchema.plugin(autoIncrement.plugin, 'groups');

    const products = mongoose.model('products', newSchema);

    return products;

};